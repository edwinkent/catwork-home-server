package cn.springlogic.workcase.rest.projection;

import java.util.List;

/**
 * Created by kentkent on 2017/4/25.
 */
public interface WorkCaseProjection {
    Long getId();
    String getTitle();
    List<ProjectProjection> getProjects();
}
