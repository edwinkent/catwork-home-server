package cn.springlogic.workcase.rest.projection;

import org.springframework.beans.factory.annotation.Value;

/**
 * Created by kentkent on 2017/4/25.
 */
public interface ProjectTypeProjection {
    Long getId();
    String getName();

    @Value("#{target.projects.size()}")
    int getProTotal();
}
