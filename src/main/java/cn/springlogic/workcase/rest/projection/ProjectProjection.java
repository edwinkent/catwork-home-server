package cn.springlogic.workcase.rest.projection;

/**
 * Created by kentkent on 2017/4/25.
 */
public interface ProjectProjection {
    Long getId();
    String getName();
    ProjectTypeProjection getType();
}
