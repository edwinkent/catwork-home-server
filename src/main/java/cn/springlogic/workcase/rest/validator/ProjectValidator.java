package cn.springlogic.workcase.rest.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by kentkent on 2017/4/23.
 */
public class ProjectValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }

    @Override
    public void validate(Object o, Errors errors) {
        System.out.print(o.toString());
    }
}
