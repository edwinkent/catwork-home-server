package cn.springlogic.workcase.rest.handler;

import cn.springlogic.workcase.jpa.entity.Project;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Created by kentkent on 2017/4/23.
 */
@Component
@RepositoryEventHandler(Project.class)
public class ProjectEventHandler {


    @HandleBeforeCreate
    public void afterCreate (Project project) {
        System.out.print("创建了一个新项目" + project.toString());
        Object o = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.print(o);
    }
}
