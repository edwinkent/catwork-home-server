package cn.springlogic.workcase.jpa.repository;

import cn.springlogic.workcase.jpa.entity.ProjectType;
import cn.springlogic.workcase.rest.projection.ProjectTypeProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by figo-007 on 2017/2/22.
 */
@RepositoryRestResource(path = "work-case:project-types", excerptProjection = ProjectTypeProjection.class)
public interface ProjectTypeRepository extends JpaRepository<ProjectType, Long> {
}
