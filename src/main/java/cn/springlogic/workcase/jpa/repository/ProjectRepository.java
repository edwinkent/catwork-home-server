package cn.springlogic.workcase.jpa.repository;

import cn.springlogic.workcase.jpa.entity.Project;
import cn.springlogic.workcase.rest.projection.ProjectProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by kentkent on 2017/4/22.
 */
@RepositoryRestResource(path = "work-case:projects", excerptProjection = ProjectProjection.class)
public interface ProjectRepository extends JpaRepository<Project, Long> {
}
