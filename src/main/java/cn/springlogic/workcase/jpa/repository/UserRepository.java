package cn.springlogic.workcase.jpa.repository;

import cn.springlogic.workcase.jpa.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by kentkent on 2017/4/20.
 */
@RepositoryRestResource
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(@Param("username") String username);
}
