package cn.springlogic.workcase.jpa.repository;

import cn.springlogic.workcase.jpa.entity.WorkCase;
import cn.springlogic.workcase.rest.projection.WorkCaseProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by kentkent on 2017/2/14.
 */
@RepositoryRestResource(path = "work-case:work-cases", excerptProjection = WorkCaseProjection.class)
public interface WorkCaseRepository extends JpaRepository<WorkCase, Long> {
}
