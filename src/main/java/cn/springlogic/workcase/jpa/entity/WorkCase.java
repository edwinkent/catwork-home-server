package cn.springlogic.workcase.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kentkent on 2017/1/30.
 */
@Entity
@Data
public class WorkCase {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 255)
    private String title;

    @OneToMany(mappedBy = "workCase", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @Transient
    private String something;

    public WorkCase() {
    }

    public WorkCase(String title) {
        setTitle(title);
    }
}
