package cn.springlogic.workcase.jpa.entity;

import lombok.Data;
import org.springframework.data.rest.core.annotation.RestResource;

import javax.persistence.*;

/**
 * Created by kentkent on 2017/2/18.
 */
@Entity
@Data
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 255)
    private String name;

    @ManyToOne
    private ProjectType type;

    @RestResource(path = "work-case")
    @ManyToOne
    private WorkCase workCase;

    public Project () {
    }

    public Project (String name) {
        setName(name);
    }

    public Project (String name, ProjectType projectType) {
        setName(name);
        setType(projectType);
    }

    public Project (String name, ProjectType projectType, WorkCase workCase) {
        setName(name);
        setType(projectType);
        setWorkCase(workCase);
    }

    public Project (String name, WorkCase workCase) {
        setName(name);
        setWorkCase(workCase);
    }
}
