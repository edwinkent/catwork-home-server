package cn.springlogic.workcase.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kentkent on 2017/2/18.
 */
@Entity
@Data
public class ProjectType {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 255)
    private String name;

    @OneToMany(mappedBy = "type", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    public int getProjectTotal() {
        return this.projects.size();
    }

    public ProjectType () {
    }

    public ProjectType (String name) {
        setName(name);
    }
}
