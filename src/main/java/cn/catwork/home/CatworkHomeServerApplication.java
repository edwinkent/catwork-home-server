package cn.catwork.home;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication(scanBasePackages = {"cn.catwork.home", "cn.springlogic"})
@EnableJpaRepositories(basePackages = {"cn.springlogic"})
@EntityScan(basePackages = {"cn.springlogic"})
@EnableAuthorizationServer
@EnableResourceServer
public class CatworkHomeServerApplication {

    private static CatworkHomeDataGenerator catworkHomeDataGenerator;

    @Autowired
    private void setCatworkHomeDataGenerator(CatworkHomeDataGenerator catworkHomeDataGenerator) {
        CatworkHomeServerApplication.catworkHomeDataGenerator = catworkHomeDataGenerator;
    }

    public static void main(String[] args) {
        SpringApplication.run(CatworkHomeServerApplication.class, args);
        CatworkHomeServerApplication.catworkHomeDataGenerator.generate();
    }
}
