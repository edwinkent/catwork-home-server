package cn.catwork.home;

import cn.springlogic.workcase.jpa.entity.Project;
import cn.springlogic.workcase.jpa.entity.ProjectType;
import cn.springlogic.workcase.jpa.entity.User;
import cn.springlogic.workcase.jpa.entity.WorkCase;
import cn.springlogic.workcase.jpa.repository.WorkCaseRepository;
import cn.springlogic.workcase.jpa.repository.ProjectRepository;
import cn.springlogic.workcase.jpa.repository.ProjectTypeRepository;
import cn.springlogic.workcase.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by kentkent on 2017/4/22.
 */
@Component
public class CatworkHomeDataGenerator {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProjectTypeRepository projectTypeRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private WorkCaseRepository workCaseRepository;

    CatworkHomeDataGenerator() {
    }

    public void generate() {

        // 添加用户帐号
        userRepository.save(new User("kent","ekpass"));
        userRepository.save(new User("kent2","ekpass"));

        // 添加项目类型
        ProjectType projectTypePc = new ProjectType("PC网站");
        ProjectType projectTypeWap = new ProjectType("手机网站");
        ProjectType projectTypeWx = new ProjectType("微信公众号");
        projectTypeRepository.save(projectTypePc);
        projectTypeRepository.save(projectTypeWap);
        projectTypeRepository.save(new ProjectType("苹果APP"));
        projectTypeRepository.save(new ProjectType("安卓APP"));
        projectTypeRepository.save(projectTypeWx);
        projectTypeRepository.save(new ProjectType("微信小程序"));

        // 添加案例
        WorkCase workCase = new WorkCase("彩怒利");
        workCaseRepository.save(workCase);

        // 添加项目
        projectRepository.save(new Project("莱菲蔓官方网站", projectTypePc, workCase));
        projectRepository.save(new Project("莱菲蔓官方手机网站", projectTypeWap, workCase));
        projectRepository.save(new Project("莱菲蔓官方微信网站", projectTypeWx, workCase));

    }
}
