package cn.catwork.home.config;

import cn.springlogic.workcase.jpa.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

/**
 * Created by kentkent on 2017/4/20.
 */
@Component
public class CatworkUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        UserDetails userDetails = userRepository.findByUsername(s);
        if (userDetails == null) throw new UsernameNotFoundException("用户不存在");

        return userDetails;
    }
}
